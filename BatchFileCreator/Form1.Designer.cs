﻿namespace BatchWithImageAndManualRegionFiles
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddSample = new System.Windows.Forms.Button();
            this.SampleList = new System.Windows.Forms.ListView();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddSample
            // 
            this.btnAddSample.Location = new System.Drawing.Point(25, 16);
            this.btnAddSample.Name = "btnAddSample";
            this.btnAddSample.Size = new System.Drawing.Size(75, 23);
            this.btnAddSample.TabIndex = 0;
            this.btnAddSample.Text = "Add Sample";
            this.btnAddSample.UseVisualStyleBackColor = true;
            this.btnAddSample.Click += new System.EventHandler(this.btnAddSample_Click);
            // 
            // SampleList
            // 
            this.SampleList.Location = new System.Drawing.Point(15, 60);
            this.SampleList.Name = "SampleList";
            this.SampleList.Size = new System.Drawing.Size(1028, 463);
            this.SampleList.TabIndex = 1;
            this.SampleList.UseCompatibleStateImageBehavior = false;
            this.SampleList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SampleList_MouseClick);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.Location = new System.Drawing.Point(124, 17);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(75, 23);
            this.btnSaveFile.TabIndex = 2;
            this.btnSaveFile.Text = "Save File";
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 535);
            this.Controls.Add(this.btnSaveFile);
            this.Controls.Add(this.SampleList);
            this.Controls.Add(this.btnAddSample);
            this.Name = "Form1";
            this.Text = "BatchFileCreator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddSample;
        private System.Windows.Forms.ListView SampleList;
        private System.Windows.Forms.Button btnSaveFile;
    }
}

