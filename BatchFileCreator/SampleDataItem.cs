﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchWithImageAndManualRegionFiles
{
    public class SampleDataItem
    {
        public string m_ImageFolderName = "";
        public string m_ImageFileName = "";
        public string m_RegionFileName = "";
    }
}
